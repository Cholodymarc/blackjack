package pl.cholodymarc.blackjack.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import pl.cholodymarc.blackjack.model.dto.PlayerDto;
import pl.cholodymarc.blackjack.model.game.Deck;
import pl.cholodymarc.blackjack.model.game.House;

@Controller
@SessionAttributes({"deck", "house", "player"})
public class SetUpController {


    @GetMapping("/setup")
    public String loginView(){
        return "redirect:/game";
    }

    @ModelAttribute("deck")
    public Deck createEmptyDeck(){
        return new Deck();
    }

    @ModelAttribute("player")
    public PlayerDto createPlayer(){
        return new PlayerDto();
    }

    @ModelAttribute("house")
    public House createHouse(){
        return new House();
    }
}
