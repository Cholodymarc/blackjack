package pl.cholodymarc.blackjack.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.cholodymarc.blackjack.model.dto.PlayerDto;
import pl.cholodymarc.blackjack.service.PlayerService;

@Controller
public class PlayerController {

    @Autowired
    private PlayerService playerService;

    @GetMapping("/signup")
    public ModelAndView signUpView(){
        return new ModelAndView("signup", "playerToInsert", new PlayerDto());
    }

    @PostMapping("/signup")
    public String addPlayer(@ModelAttribute PlayerDto playerDto){
        playerService.addPlayer(playerDto);
        return "redirect:/login";
    }
}
