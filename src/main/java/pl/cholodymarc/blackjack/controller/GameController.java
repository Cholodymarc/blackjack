package pl.cholodymarc.blackjack.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import pl.cholodymarc.blackjack.model.dto.PlayerDto;
import pl.cholodymarc.blackjack.model.game.Deck;
import pl.cholodymarc.blackjack.model.game.House;

@Controller
public class GameController {

    @GetMapping("/game")
    public String gameView(
            Model model,
            @SessionAttribute("deck") Deck deck,
            @SessionAttribute("player") PlayerDto player,
            @SessionAttribute("house") House house
    ) {
        model.addAttribute("deck", deck);
        model.addAttribute("player", player);
        model.addAttribute("house", house);
        return "game";
    }
}
