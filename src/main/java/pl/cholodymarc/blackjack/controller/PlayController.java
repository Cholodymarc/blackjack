package pl.cholodymarc.blackjack.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import pl.cholodymarc.blackjack.model.dto.PlayerDto;
import pl.cholodymarc.blackjack.model.game.Deck;
import pl.cholodymarc.blackjack.model.game.House;
import pl.cholodymarc.blackjack.service.DeckService;
import pl.cholodymarc.blackjack.service.GameService;

@Controller
public class PlayController {

    @Autowired
    private DeckService deckService;

    @Autowired
    private GameService gameService;

    @PostMapping("/play")
    public String playTheGame(
            @SessionAttribute("deck") Deck deck,
            @SessionAttribute("player") PlayerDto player,
            @SessionAttribute("house") House house
            ) {
        deckService.generateDeck(deck);
        gameService.startTheGame(deck, player, house);
        if(gameService.isBusted(player)){
            return "/defeat";
        } else if (gameService.isWinner(player)){
            return "/victory";
        }
        return "redirect:/game";
    }
}

