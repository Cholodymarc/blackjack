package pl.cholodymarc.blackjack.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttribute;
import pl.cholodymarc.blackjack.model.dto.PlayerDto;
import pl.cholodymarc.blackjack.model.game.Deck;
import pl.cholodymarc.blackjack.model.game.House;
import pl.cholodymarc.blackjack.service.GameService;

@Controller
public class StandController {

    @Autowired
    private GameService gameService;

    @PostMapping("/stand")
    public String hit(
            @SessionAttribute("deck") Deck deck,
            @SessionAttribute("player") PlayerDto player,
            @SessionAttribute("house") House house
    ) {
        gameService.stand(deck, player, house);
        return "redirect:/game";
    }
}
