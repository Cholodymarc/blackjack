package pl.cholodymarc.blackjack.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.cholodymarc.blackjack.model.entity.Player;

public interface PlayerRepository extends JpaRepository<Player, Long> {

}
