package pl.cholodymarc.blackjack.model.entity;

import javax.persistence.*;

@Entity
public class Ranking {

    @Id
    private Long id;

    private Long points;

    @OneToOne
    private Player player;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }
}
