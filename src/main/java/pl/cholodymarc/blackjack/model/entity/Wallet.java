package pl.cholodymarc.blackjack.model.entity;

import javax.persistence.*;

@Entity
public class Wallet {

    @Id
    private Long id;
    private Long cashAmount;

    @OneToOne
    private Player player;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(Long cashAmount) {
        this.cashAmount = cashAmount;
    }
}
