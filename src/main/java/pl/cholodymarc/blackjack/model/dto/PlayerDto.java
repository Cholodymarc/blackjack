package pl.cholodymarc.blackjack.model.dto;

import pl.cholodymarc.blackjack.model.game.GenericPlayer;
import pl.cholodymarc.blackjack.model.game.Hand;

public class PlayerDto implements GenericPlayer {

    private Long id;
    private String login;
    private String password;
    private String role;
    private Hand hand;
    private boolean isBusted;
    private boolean won;

    public PlayerDto() {
        this.hand = new Hand();
    }

    public PlayerDto(String login) {
        this.login = login;
    }

    @Override
    public Hand getHand() {
        return hand;
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isBusted() {
        return isBusted;
    }

    public void setBusted(boolean busted) {
        isBusted = busted;
    }

    public boolean isWon() {
        return won;
    }

    public void setWon(boolean won) {
        this.won = won;
    }

    @Override
    public String toString() {
        return "PlayerDto{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", hand=" + hand +
                '}';
    }
}
