package pl.cholodymarc.blackjack.model.game;

public class House implements GenericPlayer {
    private Hand hand;

    public House() {
        this.hand = new Hand();
        hand.setName("Hand of House");
    }

    @Override
    public Hand getHand() {
        return hand;
    }
}
