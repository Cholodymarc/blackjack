package pl.cholodymarc.blackjack.model.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
    private List<Card> cards;

    public Deck() {
        this.cards = new ArrayList<>();
    }

    public void generateDeck(){
        cards.clear();
        populate();
        shuffle();
    }

    private void populate(){
        for(Color color: Color.values()){
            for(Rank rank: Rank.values()){
                this.cards.add(new Card(rank, color));
            }
        }
    }

    private void shuffle(){
        Collections.shuffle(this.cards);
    }

    public void dealCardTo(GenericPlayer genericPlayer, Boolean isFaceUp){
        Card cardToDeal = this.cards.get(this.cards.size()-1);
        cardToDeal.setFaceUp(isFaceUp);
        genericPlayer.getHand().add(cardToDeal);
        this.cards.remove(this.cards.size()-1);
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

}
