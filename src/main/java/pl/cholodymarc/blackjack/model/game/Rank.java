package pl.cholodymarc.blackjack.model.game;

public enum Rank {
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    J(2),
    Q(3),
    K(4),
    A(11)
    ;

    private Integer value;

    Rank(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }
}
