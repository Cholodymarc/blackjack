package pl.cholodymarc.blackjack.model.game;

public class Card {
    private Rank rank;
    private Color color;
    private boolean isFaceUp;

    public Card(Rank rank, Color color) {
        this.rank = rank;
        this.color = color;
        this.isFaceUp = false;
    }

    public Rank getRank() {
        return rank;
    }

    public Color getColor() {
        return color;
    }

    public boolean getIsFaceUp() {
        return isFaceUp;
    }

    public void setFaceUp(boolean faceUp) {
        isFaceUp = faceUp;
    }

    @Override
    public String toString() {
        return "Card{" +
                "rank=" + rank +
                ", color=" + color +
                ", isFaceUp=" + isFaceUp +
                '}';
    }
}
