package pl.cholodymarc.blackjack.model.game;

import java.util.ArrayList;
import java.util.List;


public class Hand {
    public static final byte MAX_CARDS_VALUE = 21;
    private String name;
    private List<Card> cards;

    public Hand() {
        this.cards = new ArrayList<>();
    }

    public void clearHand(){
        cards.clear();
    }

    public void add(Card card){
        this.cards.add(card);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public List<Card> getCards() {
        return cards;
    }
}
