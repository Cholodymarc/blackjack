package pl.cholodymarc.blackjack.model.game;

public interface GenericPlayer {
    Hand getHand();
}
