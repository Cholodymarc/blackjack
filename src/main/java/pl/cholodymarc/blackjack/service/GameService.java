package pl.cholodymarc.blackjack.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.cholodymarc.blackjack.model.dto.PlayerDto;
import pl.cholodymarc.blackjack.model.game.*;

@Service
public class GameService {

    public void startTheGame(Deck deck, PlayerDto player, House house){
        player.getHand().clearHand();
        house.getHand().clearHand();
        deck.dealCardTo(house,false);
        deck.dealCardTo(house,true);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        player.setLogin(authentication.getName());
        player.getHand().setName("Hand of " + player.getLogin());
        deck.dealCardTo(player,true);
        deck.dealCardTo(player,true);
    }

    public void hit(Deck deck, PlayerDto player, House house){
        house.getHand().getCards().forEach(c->c.setFaceUp(true));
        deck.dealCardTo(player, true);
    }

    public void stand(Deck deck, PlayerDto player, House house){

    }

    public boolean isWinner(GenericPlayer player){
        byte playerCardsValue = getPlayerCardsValue(player);
        return playerCardsValue == Hand.MAX_CARDS_VALUE;
    }

    public boolean isBusted(GenericPlayer player){
        return getPlayerCardsValue(player) > Hand.MAX_CARDS_VALUE;
    }

    public byte getPlayerCardsValue(GenericPlayer player){
        byte result = 0;
        for( Card c : player.getHand().getCards()){
            result += c.getRank().getValue();
        }
        return result;
    }
}
