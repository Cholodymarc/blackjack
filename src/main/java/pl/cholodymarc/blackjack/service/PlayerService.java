package pl.cholodymarc.blackjack.service;

import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.cholodymarc.blackjack.model.dto.PlayerDto;
import pl.cholodymarc.blackjack.model.entity.Player;
import pl.cholodymarc.blackjack.model.game.House;
import pl.cholodymarc.blackjack.repository.PlayerRepository;

@Service
public class PlayerService {

    private ModelMapper modelMapper;
    private PlayerRepository playerRepository;
    private PasswordEncoder passwordEncoder;

    public PlayerDto createPlayer(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return new PlayerDto(authentication.getName());
    }

    public House createHouse(){
        return new House();
    }

    public PlayerService(ModelMapper modelMapper, PlayerRepository playerRepository, PasswordEncoder passwordEncoder) {
        this.modelMapper = modelMapper;
        this.playerRepository = playerRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void addPlayer(PlayerDto playerDto) {
        Player player = modelMapper.map(playerDto, Player.class);
        player.setPassword(passwordEncoder.encode(player.getPassword()));
        player.setRole("ROLE_USER");
        playerRepository.save(player);
    }
}
