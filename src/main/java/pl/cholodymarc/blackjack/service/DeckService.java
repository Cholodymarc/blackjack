package pl.cholodymarc.blackjack.service;

import org.springframework.stereotype.Service;
import pl.cholodymarc.blackjack.model.game.Deck;

@Service
public class DeckService {

    public Deck createDeck(){
        Deck deck = new Deck();
        deck.generateDeck();
        return deck;
    }

    public void generateDeck(Deck deck){
        deck.generateDeck();
    }
}
